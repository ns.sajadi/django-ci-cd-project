#!/bin/sh

echo DEBUG=True >> .env
echo SQL_ENGINE=django.db.backends.postgresql >> .env
echo DATABASE=postgres >> .env

echo SECRET_KEY='django-insecure-secret-key' >> .env
echo SQL_DATABASE=db_table >> .env
echo SQL_USER=db_user >> .env
echo SQL_PASSWORD=db_pass >> .env
echo SQL_HOST=ci_cd_db >> .env
echo SQL_PORT=5432 >> .env
echo POSTGRES_USER=db_user >> .env
echo POSTGRES_PASSWORD=db_pass >> .env
echo POSTGRES_DB=db_table >> .env
echo DB_IMAGE=$IMAGE:db  >> .env
echo WEB_IMAGE=$IMAGE:web  >> .env
echo NGINX_IMAGE=$IMAGE:nginx  >> .env
echo CI_REGISTRY_USER=$CI_REGISTRY_USER   >> .env
echo CI_REGISTRY=$CI_REGISTRY  >> .env
echo IMAGE=$CI_REGISTRY/$CI_PROJECT_NAME >> .env
